using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private Vector3 startingPosition;
    private float shakeTimer;

    private float shakeDuration;
    public float shakeAmount = 0.2f;

    // Start is called before the first frame update
    void Start()
    {
        startingPosition = transform.position;
        shakeDuration = 1;
    }

    public void ShakeCamera(float duration, float shakeFactor)
    {
        StopCoroutine("Shake");
        transform.position = startingPosition;

        shakeDuration = duration;
        shakeAmount = shakeFactor;

        StartCoroutine("Shake");
    }

    IEnumerator Shake()
    {
        shakeTimer = 0; 

        while (shakeTimer < shakeDuration)
        {
            shakeTimer += Time.deltaTime;
            transform.localPosition = startingPosition + Random.insideUnitSphere * shakeAmount;

            yield return null;
        }
        transform.localPosition = startingPosition;

        /*
        while (shakeTimerY < 1)
        {
            yield return null;

            shakeTimerY += Time.deltaTime;
            transform.position = new Vector3(startingPosition.x + shakeTimerX, startingPosition.y + shakeTimerY, startingPosition.z);
        }

        //shakeTimer = 0; // camera at 9,6

        while (shakeTimerX < 1)
        {
            yield return null;

            shakeTimerX += Time.deltaTime;
            shakeTimerY -= Time.deltaTime;
            transform.position = new Vector3(startingPosition.x + shakeTimerX, startingPosition.y + shakeTimerY, startingPosition.z);
        }
        // camera at 10, 5

        while (shakeTimerX > 0)
        {
            shakeTimerX -= Time.deltaTime;
            shakeTimerY -= Time.deltaTime;
            transform.position = new Vector3(startingPosition.x + shakeTimerX, startingPosition.y + shakeTimerY, startingPosition.z);

            yield return null;
        }
        // camera at 9, 4

        while (shakeTimerY < 1)
        {
            shakeTimerX += Time.deltaTime;
            shakeTimerY += Time.deltaTime;
            transform.position = new Vector3(startingPosition.x + shakeTimerX, startingPosition.y + shakeTimerY, startingPosition.z);

            yield return null;
        }
            // camera at 8, 5

        while (shakeTimerX < 1)
        {
            shakeTimerX += Time.deltaTime;
            transform.position = new Vector3(startingPosition.x + shakeTimerX, startingPosition.y + shakeTimerY, startingPosition.z);

            yield return null;
        }
        // camera at 9, 5
        */
    }
}
