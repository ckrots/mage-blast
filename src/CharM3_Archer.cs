using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharM3_Archer : CharM3
{
    // Use this for initialization
    void Start ()
    {
        Inits();
        animations = new string[] { "idle", "jump","run","shoot","walk", "dying"};

        hp = 10;
        mana = 10;
        ac = 25;
    }
}
