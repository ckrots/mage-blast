using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleLingering : MonoBehaviour
{
    private ParticleSystem ps;

    public void AboutToDie()
    {
        transform.parent = null;
        //GetComponent<ParticleSystem>().emissionRate = 0;
        ps = GetComponent<ParticleSystem>();

        var emission = ps.emission;
        emission.rateOverTime = 0;

        StartCoroutine(StartDeathTimer());
    }

    IEnumerator StartDeathTimer()
    {
        yield return new WaitForSeconds(1.0f);
        Destroy(this.gameObject);
    }
}
