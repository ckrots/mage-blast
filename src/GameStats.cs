using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStats : MonoBehaviour
{
    private List<int> idList = new List<int>();
    private List<string> mageNameList = new List<string>();
    private List<int> killsList = new List<int>();
    private List<int> rank = new List<int>();
    private int killIndex;
    private int totalRanks;

    public void InitMage(int id, string name)
    {
        idList.Add(id);
        mageNameList.Add(name);
        killsList.Add(0);
        rank.Add(0);

        killIndex = idList.Count;
        totalRanks = idList.Count;
    }

    public int GetTotalRanks()
    {
        return totalRanks;
    }

    public void AddKillCount(int id)
    {
        int index = FindIndexByID(id);

        if (index == 999)
            Debug.Log("ID not found - FindIndexByID in GameStats");
        else
            killsList[index]++;
    }

    public void SetRank(int id)
    {
        int index = FindIndexByID(id);

        if (index == 999)
            Debug.Log("ID not found - FindIndexByID in GameStats");
        else if (index < 0)
            Debug.Log("SetDeathOrder - KillIndex invalid - Index is < 0");
        else
        {
            rank[index] = killIndex;
            killIndex--;

            if (killIndex == 1)
            {
                int i = 0;

                while (i < idList.Count)
                {
                    if (rank[i] == 0)
                        rank[i] = 1;
                    else
                        i++;
                }
            }
        }
    }

    private int FindIndexByID(int id)
    {
        int i = 0;

        while (i < idList.Count)
        {
            if (idList[i] == id)
                return i;
            else
                i++;
        }

        return 999;
    }

    public string GetNameByRank(int reqRank)
    {
        int i = 0;

        while (i < idList.Count)
        {
            if (rank[i] == reqRank)
                return mageNameList[i];
            else
                i++;
        }

        return "";
    }

    public int GetKillsByRank(int reqRank)
    {
        int i = 0;

        while (i < idList.Count)
        {
            if (rank[i] == reqRank)
                return killsList[i];
            else
                i++;
        }

        return 0;
    }
}
