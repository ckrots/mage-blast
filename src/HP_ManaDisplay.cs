using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HP_ManaDisplay : MonoBehaviour
{
    public CharM3 CharM3;
    public Transform hpSprite;
    public Transform manaSprite;

    private Vector3 originalHPScale;
    private Vector3 originalManaScale;

    // Start is called before the first frame update
    void Start()
    {
        CharM3 = GetComponentInParent<CharM3>();

        if (hpSprite != null)
            originalHPScale = hpSprite.transform.localScale;
        if (manaSprite != null)
            originalManaScale = manaSprite.transform.localScale;
    }

    public void Update()
    {
        //show image at scale hp/maxHp & mana/maxMana
        if (hpSprite != null)
            hpSprite.transform.localScale = new Vector3(
                originalHPScale.x * ((float)CharM3.GetHP() / CharM3.GetMaxHp()),
                originalHPScale.y, 
                originalHPScale.z);

        if (manaSprite != null)
            manaSprite.transform.localScale = new Vector3(
                originalManaScale.x * ((float)CharM3.GetMana() / CharM3.GetMaxMana()), 
                originalManaScale.y, originalManaScale.z);
    }
}
