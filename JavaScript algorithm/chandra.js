CalculateActionAbility();
//CalculateMove();

function CalculateActionAbility()
{
    if(myMage.isShieldOn){
        attack();
        SetMoveAction(false);
    } else if(myMage.mana > 5 && myMage.hp > 5){
        attack();
        SetMoveAction(false);
    } else if(myMage.mana > 5 && myMage.hp < 5){
        SetAbilityAction('iceshield');
        SetRandomMove();
    } else if(myMage.mana < 5 && myMage.hp > 5){
        SetAbilityAction("nothing");
        SetMoveAction(false);  
    } else if(myMage.mana < 5 && myMage.hp < 5 && !myMage.isShieldOn){
        SetAbilityAction('blink');
        SetRandomMove();
    } else {
        if(myMage.mana < 3){
            SetAbilityAction("nothing");
            SetMoveAction(false);
        } else if(myMage.mana > myMage.hp){
            attack();
        }
    }
}

function attack(){
    var atID ;
    var atType;
    var ceID = GetClosestOpponent_ID();
    var leID = GetLowestHealthOpponent_ID();
    if(enemy_isShieldOn[leID] && enemy_isShieldOn[ceID]){
        var total = GetCurrentOpponentCount();
        var i;           
        for(i=0; i < total; i++){
            if(!enemy_isShieldOn[i] && enemy_mana[i] < 5){
                atID = i; atType="drainmana";
            } else if(!enemy_isShieldOn[i] && enemy_hp[i] < 5){
                atID = i; atType="snowball";
            }
        }
        SetAbilityAction(atType, atID);
    } else if(enemy_isShieldOn[ceID]){
        SetAbilityAction("frostbolt", leID);
    } else SetAbilityAction("frostbolt", ceID);
}