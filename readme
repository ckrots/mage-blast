Mage Blast is a programming game in which the user controls their avatar via JavaScript in a battle royale death match against other programmers.  This event is for our 3-team trio (D&I, DevSupport, ArchNerds).  Final day of competition will take place at the historic Burr House (210 E Chestnut St, Bloomington).  Cosplay is encouraged, but optional.

Tournament format:
	A battle will consist of 10 total ‘teams’ battling.  
	A team can be an individual or multiple people, but represents a single algorithm.
	Brackets will be created if necessary
	Each team/player will have a week to DL the game and perfect their algorithm on their PERSONAL DEVICES (Windows, Linux, and Mac supported)
	Source code to the game files will be available for research, understanding, and/or exploitation

Game Objective and Framework Overview:
    The objective of the game is to be the lone surviving player.  This will require a combination of offensive and defensive tactics.
    
    The game design is simple:
        Each javascript algoritm executes in a standalone javescript engine instance, tied to a specific mage.  
        At the beginning of each round, every javascript engine instance is updated with the current global variable data (ie, your enemy data).
        ALL javascript engine instances are then executed.  The execution determines the ability and the movement of that mage for that round.
        All ability actions occur simultaneously.  The animation/casting time is 1 second.
        After the ability is complete, all movements occur simultaneously.  The movement lasts 1 second.
        The round is over (2 secs total time) and the loop continues, until only 1 mage remains!

Game Attributes:
	Each mage starts with:
		10 mana/energy/resources
		10 hit points (hp)
		
	Both hp and mana have a maximum value of 10.
	
	The playable grid is (0,0) to (13,8).  Mages cannot move outside this grid.
	Attack abilities travel 8 units

	Each round consists of a single action followed by a single movement
		Action - SetAbilityAction(string action_name, int details):
			Details can be multiple things, depending on the action.
				For attack abilities, details = target_ID
				For blink, details represents the angle to move (see notes below on calculating movement angle)
				For nothing (no action), details is an optional and unused parameter
			
		Movement - SetMoveAction(bool do_you_want_to_move, int angle_of_movement):
			Movement angles are a 360 degree arc.  North is 0 and angles proceed clockwise.  
            Values are rounded to the nearest 45 degree angle (ie, SetMoveAction(true, 27) results in a movement at angle 45 (North East))
				North = 0
				East = 90
				South = 180
				West = 270
		
		Mana Regeneration:
			If no action is taken, the mage regenerates 2 mana.  
			If no action is taken AND no movement is taken, the mage regenerates 5 mana.

Scripting - Global variables are updated and the user script is executed once per round.  The script should determine both ability and movement actions based on the available data.  
	Abilities:
		Abilites are string values
		NOT case sensitive
		Allowed values: blink, drainmana, frostbolt, iceshield, snowball, nothing
		Example: SetAbilityAction("fRoStBoLt", 1234);
		Example: SetAbilityAction("NOTHING");
		
	Movement:
		Movement function accepts a bool, indicating desire to move
		Angle of movement is described above
		Example: SetMoveAction(true, 90);
		Example: SetMoveAction(false);
		
	Global Variables:
		YOUR mage data is present in the myMage struc:
			myMage.id; 			// no value in this, don't use it
			myMage.x;			// x coordinate
			myMage.y;			// y coordinate
			myMage.hp;			// current hit points (health)
			myMage.mana;		// current mana
			myMage.isShieldOn;	// bool representing your shield status

		Enemy Data is an array.  
        note - while these values are not within a single struc in JavaScript, bucket[x] represents the same enemy across arrays:
			enemy_id[]; 		// unique identifier used for offensive abilities
			enemy_x[];			// x coordinate
			enemy_y[];			// y coordinate
			enemy_hp[];			// current hit points (health)
			enemy_mana[];		// current mana
			enemy_isShieldOn[];	// bool representing your shield status
	
	DebugLog:
		During testing, you can include DebugLog in your scripts.
        After a round is complete the DebugLog can be saved to the harddrive.  
		DebugLog accepts a string.
		DebugLog is capped at 1GB (1 billion characters logged)
	
	Helper Functions:
		GetCurrentOpponentCount() returns the number of competitors alive.  May be useful in your array loops?
		GetClosestOpponent_ID() returns the ID of the closest competitor.  May be a good candidate to attack?
		GetLowestHealthOpponent_ID() returns the ID of the competitor with the lowest health.  May be a good candidate to attack?
		SetRandomAttack() has equal chance of setting any of the ability values, and selects a random target or random angle as applicable.  Sometimes you just don't know what to do!
		SetRandomMove() has 50/50 chances of setting the Move function, and selects a random angle if applicable.  May be useful in testing?

Abilities & Movement details (with examples):
	Frostbolt - A powerful offensive ability of pure frozen wrath.
		Cost: 5 mana
		Effect on impact: 5 damage
		Example cast: SetAbilityAction("frostbolt", enemy_id[7]);

	Snowball - A fist sized ball of packed snow; an efficient but less powerful offensive ability.
		Cost: 3 mana
		Effect on impact: 3 damage
		Example cast: SetAbilityAction("snowball", GetLowestHealthOpponent_ID() );
		
	Drain Mana - Phsychic energy that drains resources but leaves no physical mark.
		Cost: 3 mana
		Effect on impact: Removes 5 mana
		Example cast: SetAbilityAction("drainmana", GetClosestOpponent_ID() );
		
	Ice Shield - A defensive shield surrounds the mage, negating the next harmful spell received.
		Cost: 4 mana
		Effect on impact: n/a
		Example cast: SetAbilityAction("iceshield");
		
	Blink - Twisting the variables of spacetime, an instant teleportation in any direction.
		Cost: 2 mana
		Effect on impact: none
		Example cast: SetAbilityAction("blink", 180);
		
	Move - Walk
		Cost: n/a
		Effect on impact: none
		Example cast: SetMoveAction(true, 180);	// just now realizing I should remove the bool to active, 
                                                // and can have it init to false each round.  Will have that before next week.	
	
